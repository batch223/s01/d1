<?php

// [SECTION] Comments
// crtl + / (single line)
// ctrl + shift + / (multiline)

// [SECTION] Variables
// Variables are dfined using ($) notation before the name of the variable.
$name = 'John Smith';
$email = 'johnsmith@mail.com';

// [SECTION]
// Constatns are defined using the 'define()' functions.
// Naming convention for 'constants' should be in ALL CAPS
// Doesnt use the $  notation before the variables

define('PI', 3.1416);

// [SECTION] Data Types

// Strings
$state = 'New York';
$country = 'United States of America';
$address = $state.', '.$country; // concatination via dot (.) operator.
$address = "$state, $country"; // concatenation using double qoutes.

// Integer
$age = 31;
$headcount = 26;

//Floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

// Boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

// Null
$spouse =  null;
$middleName = null;

// Objects
$gradeObj = (object) [
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object) [
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object) [
		'state' => 'New York',
		'country' => 'United States of America'
	]
];

// [SECTION] Operators

// Assignment Operators
$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// [SECTION] Functions
function getFullName($firstName, $middleInitial, $lastName) {
	return "$lastName, $firstName, $middleInitial";
}

// [SECTION] If-ElseIf-Else Statement

function determineTyphoonIntensity($windSpeed) {
	if($windSpeed < 30) {
		return 'Not a typhoon yet';
	} else if ($windSpeed <= 61) {
		return 'Tropical Depression detected.';
	} else if($windSpeed >= 62 && $windSpeed <= 88) {
		return 'Tropical Strom detected.';
	} else if(windSpeed >=89 && $windSpeed <= 177) {
		return 'Severe Tropical Storm detected.';
	} else {
		return 'Typhoon detected.';
	}
}

// Conditional (Ternary) Operator
function isUnderAge($age) {
	return($age > 18) ? 'is legal age' : 'under age';
}

// Switch Statement
function determineComputerUser($computerNumber){
	switch ($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;			
		case 3:
			return 'Sid Meier';
			break;
		case 4:
			return 'Onel De Guzman';
			break;	
		case 5:
			return 'Christian Salvador';
			break;			
		default:
			return $computerNumber . ' is out of bounds.';
			break;
	}
}

// Try-Catch-Finally
function greeting($str){
	try{
		if(gettype($str)  === "string") {
			echo $str;
		}
		else {
			throw new Exception("Oops! ");
		}
	}
	catch(Exception $e) {
		echo $e->getMessage();
	}
	finally{
		echo " I did it again!";
	}
}